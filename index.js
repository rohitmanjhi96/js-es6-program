// const circle = {
//     radius: 1,
//     location: {
//         x: 1,
//         y: 1
//     },
//     draw: function() {
//         console.log('Draw');
//     }
// };
// circle.draw();

class Element {
    constructor(name, buildYear) {
        this.name = name;
        this.buildYear = buildYear;
    }
}

class Park extends Element {
    constructor(name, buildYear, area, numTrees) {
        super(name, buildYear)
        this.area = area;
        this.numTrees = numTrees;
    }

    treeDensity() {
        const density = this.numTrees / this.area;
        console.log(`${this.name} has a tree density of ${density} trees paer square km.`)
    }
}

class Street extends Element {
    constructor(name, buildYear, length, size=3) {
        super(name, buildYear)
        this.length = length;
        this.size = size;
    }
    classifyStrret() {
        const classification = new Map();
        classification.set(1, 'tiny');
        classification.set(2, 'small');
        classification.set(3, 'normal');
        classification.set(4, 'big');
        classification.set(5, 'hug');
        console.log(` ${this.name}, buid in ${this.buildYear}, is a ${classification.get(this.size)} street.`)
    }
}

const allParks = [new Park('Green park', 1987, 8.2, 215), new Park("National PArk", 1894, 2.9, 3514),
    new Park("Oak Park", 1953, 0.4, 949)];

const allStreets = [new Street("Ocean", 1989, 1.1, 4), new Street("Evergreen")]

function calc(arr) {
    const sum = arr.reduce((prev, cur, index) => prev + cur, 0);

    return [sum, sum / arr.length];
}

function reportParks(p) {
    console.log("---------PArk Report------");

    // Density
    p.forEach(el => el.treeDensity());
    //Average Age
    const ages = p.map(el => new Date().getFullYear() - el.buildYear)
    const [totalAge, avgAge] = calc(ages);
    console.log(`our ${p.length} parks have an average of ${avgAge}  years.`)
    // Which park has more than 1000 trees
    const i = p.map(el => el.numTrees).findIndex(el => el >= 1000);
    console.log(`${p[i].name} has more than 1000 trees`)

}

function reportStreet(s) {
    console.log("------------Strets Report ----");
    // Total and avg lenght of the town's
    const [totalLength, avgLength] = calc(s.map(el => el.length));
    console.log(`Our ${s.length} streets hava a total length id ${totalLength}km. with avg if ${avgLength} km`)
    // Classify sizes
    s.forEach(el => el.classifyStrret);

}

reportParks(allParks)
reportStreet(allParks)